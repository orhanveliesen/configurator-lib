package configurator.repositories;


import configurator.models.Configuration;

import java.util.Collection;


public interface ConfigurationRepository {

    public Configuration findByIsActiveAndNameAndApplicationName(Boolean isActive, String key, String applicationName);

    public Collection<Configuration> findByIsActiveAndApplicationName(Boolean isActive, String applicationName);
}
