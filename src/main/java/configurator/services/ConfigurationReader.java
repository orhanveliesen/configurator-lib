package configurator.services;


import configurator.models.Configuration;
import configurator.repositories.ConfigurationRepository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ConfigurationReader {

    private Map<String, Configuration> configurations = new HashMap<>();

    private String applicationName;

    public Integer refreshTime;

    private ConfigurationRepository configurationRepository;







    public ConfigurationReader(String applicationName, ConfigurationRepository configurationRepository, Integer refreshTime){
        this.applicationName = applicationName;
        this.configurationRepository = configurationRepository;
        this.refreshTime = refreshTime;


        startScheduler();
    }

    private void startScheduler() {

    }

    public <T> T getValue(String name, Class<T> c) {
        Configuration conf;

        try {
            conf = configurationRepository.findByIsActiveAndNameAndApplicationName(true, name, applicationName);
            configurations.put(name, conf);
        }catch (Exception e){
            //do nothing
        }

        conf = configurations.get(name);

            if (conf == null){
                return null;
            }

            if (c == String.class){
                return (T) conf.getValue();
            }else if (c == Integer.class){
                return (T) Integer.valueOf(conf.getValue());
            }else if (c == Double.class){
                return (T) Double.valueOf(conf.getValue());
            }else if (c == Boolean.class){
                return (T) Boolean.valueOf(conf.getValue());
            }else {
                throw new RuntimeException("unsupported type " + c.getSimpleName());
            }



    }


    public void refresh(){
        Collection<Configuration> configurations = configurationRepository.findByIsActiveAndApplicationName(true, applicationName);
        configurations.forEach(c -> {this.getConfigurations().put(c.getName(), c);});
    }

    public Map<String, Configuration> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(Map<String, Configuration> configurations) {
        this.configurations = configurations;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }


    public ConfigurationRepository getConfigurationRepository() {
        return configurationRepository;
    }

    public void setConfigurationRepository(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }


    public Integer getRefreshTime() {
        return refreshTime;
    }

    public void setRefreshTime(Integer refreshTime) {
        this.refreshTime = refreshTime;
    }


}
