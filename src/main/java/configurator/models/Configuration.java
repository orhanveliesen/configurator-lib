package configurator.models;

public interface Configuration {

    public Long getId() ;

    public void setId(Long id);

    public String getName();
    public void setName(String name);

    public String getType();

    public void setType(String type);

    public String getValue();

    public void setValue(String value);

    public Boolean getActive();

    public void setActive(Boolean active);

    public String getApplicationName();

    public void setApplicationName(String applicationName);
}
