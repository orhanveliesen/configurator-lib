package services;

import configurator.models.Configuration;
import configurator.repositories.ConfigurationRepository;
import configurator.services.ConfigurationReader;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConfigurationReaderTest {


    private ConfigurationRepository repo;

    private ConfigurationReader reader;
    private String applicationName = "test";
    private List<Configuration> configurations = new ArrayList<>();

    private static class ConfigurationImpl implements Configuration{

        private Long id;
        private String name;
        private String applicationName;
        private String type;
        private String value;
        private Boolean isActive;

        @Override
        public Long getId() {
            return id;
        }

        @Override
        public void setId(Long id) {
            this.id = id;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String getType() {
            return type;
        }

        @Override
        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String getValue() {
            return value;
        }

        @Override
        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public Boolean getActive() {
            return isActive;
        }

        @Override
        public void setActive(Boolean active) {
            this.isActive = active;
        }

        @Override
        public String getApplicationName() {
            return applicationName;
        }

        @Override
        public void setApplicationName(String applicationName) {
            this.applicationName = applicationName;
        }
    }

    @Before
    public void before(){
        Map<String, Object> pairs = new HashMap<>();
        pairs.put("ping", "pong");
        pairs.put("ping1", "1");
        pairs.put("pingDouble", "1.0");
        pairs.put("pingBoolean", "true");

        int i = 1;
        for (Map.Entry<String, Object> entry : pairs.entrySet()) {
            Configuration conf = new ConfigurationImpl();
            conf.setActive(true);
            conf.setApplicationName(applicationName);
            conf.setId(Long.valueOf(i));
            conf.setName(entry.getKey());
            conf.setType(entry.getValue().getClass().getSimpleName());
            conf.setValue(entry.getValue().toString());
            configurations.add(conf);
            i++;
        }

        Configuration passiveConf = new ConfigurationImpl();
        passiveConf.setActive(false);
        passiveConf.setValue("passive");
        passiveConf.setType(String.class.getSimpleName());
        passiveConf.setName("passive");
        passiveConf.setApplicationName(applicationName);
        passiveConf.setId(100L);
        configurations.add(passiveConf);



        repo = new ConfigurationRepository() {
            @Override
            public Configuration findByIsActiveAndNameAndApplicationName(Boolean isActive, String key, String applicationName) {
                List<Configuration> list = new ArrayList<>();
                for (int i = 0; i < configurations.size(); i++) {
                    Configuration conf = configurations.get(i);
                    if (conf.getName().equals(key) && conf.getApplicationName().equals(applicationName) && conf.getActive().equals(isActive)){
                        list.add(conf);
                    }
                }

                if (list.size() > 1){
                    throw new RuntimeException("There is more than one record here!");
                }
                return list.get(0);
            }

            @Override
            public Collection<Configuration> findByIsActiveAndApplicationName(Boolean isActive, String applicationName) {
                List<Configuration> list = new ArrayList<>();
                for (int i = 0; i < configurations.size(); i++) {
                    Configuration conf = configurations.get(i);
                    if ( conf.getApplicationName().equals(applicationName) && conf.getActive().equals(isActive)){
                        list.add(conf);
                    }
                }


                return list;
            }
        };
        reader = new ConfigurationReader("test", repo, 1);
    }

    @Test
    public void testGetValue() throws IOException, ClassNotFoundException {
        String pong = reader.getValue("ping", String.class);
        assertTrue(pong.equals("pong"));

        Integer pong1 = reader.<Integer>getValue("ping1", Integer.class);
        assertTrue(pong1.equals(1));

        Double pong2 = reader.<Double>getValue("pingDouble", Double.class);
        assertTrue(pong2.equals(1.0));


        Boolean pong3 = reader.<Boolean>getValue("pingBoolean", Boolean.class);
        assertTrue(pong3.equals(true));

    }

    @Test
    public void testGetValueFromCache(){

        //first load configuration in before method. then mock the repo instance
        repo = mock(ConfigurationRepository.class);
        when(repo.findByIsActiveAndNameAndApplicationName(any(), any(), any())).thenThrow(new RuntimeException("Could not get from db"));

        String pong = reader.getValue("ping", String.class);
        assertTrue(pong.equals("pong"));
    }

    @Test
    public void testPassiveConf(){
        String passive = reader.getValue("passive", String.class);
        assertTrue(Objects.isNull(passive));
    }
}
